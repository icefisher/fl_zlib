TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    zip.cpp

HEADERS += \
    zip.h

LIBS += -lz
