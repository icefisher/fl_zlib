#include <iostream>
#include <fstream>
#include <stdint.h>
#include <vector>
#include <sstream>
#include "zip.h"

using namespace std;

int main()
{
    ofstream zipStream;
    zipStream.open ("test.zip", std::ios_base::out | std::ios_base::binary);

    Zip zip(zipStream);


    char* files[] = {"/etc/passwd", "/etc/group"};


    for(int i=0; i<2; i++) {
        ifstream in(files[i], std::ios::binary | std::ios::in);

        in.seekg(0, in.end);
        size_t size = in.tellg();
        in.seekg(0, in.beg);

        std::vector<uint8_t> readBuffer;
        readBuffer.resize(size);
        in.read((char *) readBuffer.data(), size);

        stringstream ss;
        ss << "hehe";
        ss << i;

        zip.Add(ss.str().data(), (char*)readBuffer.data(), size);
    }

    return 0;
}

