#include "zip.h"
#include <zlib.h>
#include <stdio.h>
#include <assert.h>
#include <fstream>
#include <stdint.h>
#include <vector>
#include <string.h>

using namespace std;

struct LocalFileHeader
{
    uint16_t versionToExtract;
    uint16_t generalPurposeBitFlag;
    uint16_t compressionMethod;
    uint16_t modificationTime;
    uint16_t modificationDate;
    uint32_t crc32;
    uint32_t compressedSize;
    uint32_t uncompressedSize;
    uint16_t filenameLength;
    uint16_t extraFieldLength;

} __attribute__((packed));

struct CentralDirectoryFileHeader
{
    uint16_t versionMadeBy;
    uint16_t versionToExtract;
    uint16_t generalPurposeBitFlag;
    uint16_t compressionMethod;
    uint16_t modificationTime;
    uint16_t modificationDate;
    uint32_t crc32;
    uint32_t compressedSize;
    uint32_t uncompressedSize;
    uint16_t filenameLength;
    uint16_t extraFieldLength;
    uint16_t fileCommentLength;
    uint16_t diskNumber;
    uint16_t internalFileAttributes;
    uint32_t externalFileAttributes;
    uint32_t localFileHeaderOffset;

} __attribute__((packed));

struct EOCD
{
    uint16_t diskNumber;
    uint16_t startDiskNumber;
    uint16_t numberCentralDirectoryRecord;
    uint16_t totalCentralDirectoryRecord;
    uint32_t sizeOfCentralDirectory;
    uint32_t centralDirectoryOffset;
    uint16_t commentLength;

} __attribute__((packed));


Zip::Zip(std::ofstream &File) : os(File)
{
}

Zip::~Zip()
{
    // Смещение первой записи для EOCD
    const uint32_t firstOffsetCDFH = os.tellp();

    for (int i = 0; i < filenames.size(); ++i)
    {
        const std::string &filename = filenames[i];
        const FileInfo &fileInfo = fileInfoList[i];
        CentralDirectoryFileHeader cdfh;
        memset(&cdfh, 0, sizeof(cdfh));

        cdfh.compressedSize = fileInfo.compressedSize;
        cdfh.uncompressedSize = fileInfo.uncompressedSize;
        cdfh.compressionMethod = fileInfo.compressionMethod;
        cdfh.crc32 = fileInfo.crc32;
        cdfh.localFileHeaderOffset = fileInfo.offset;
        cdfh.filenameLength = filename.size();

        // Запишем сигнатуру
        const uint32_t signature = 0x02014b50;
        os.write((char *) &signature, sizeof(signature));

        // Запишем структуру
        os.write((char *) &cdfh, sizeof(cdfh));

        // Имя файла
        os.write(filename.c_str(), cdfh.filenameLength);
    }

    // Посчитаем размер данных для следующего шага
    const uint32_t lastOffsetCDFH = os.tellp();

    EOCD eocd;
    memset(&eocd, 0, sizeof(eocd));
    eocd.centralDirectoryOffset = firstOffsetCDFH;
    eocd.numberCentralDirectoryRecord = filenames.size();
    eocd.totalCentralDirectoryRecord = filenames.size();
    eocd.sizeOfCentralDirectory = lastOffsetCDFH - firstOffsetCDFH;

    // Пишем сигнатуру
    const uint32_t signature = 0x06054b50;
    os.write((char *) &signature, sizeof(signature));

    // Пишем EOCD
    os.write((char *) &eocd, sizeof(eocd));

//    os.flush();
}

void Zip::Add(const char *FileName, const char *Data, size_t DataSize)
{
    const std::string filename = FileName;
    filenames.push_back(filename);

    // Структура типа Local File Header
    LocalFileHeader lfh;
    memset(&lfh, 0, sizeof(lfh));

    // Пишем размер файла
    lfh.uncompressedSize = DataSize;

    // Считаем контрольную сумму
    lfh.crc32 = crc32(0, (unsigned char*) Data, DataSize);

    // Выделим память для сжатых данных
    dataBuffer.resize(DataSize*10);

    // Структура для сжатия данных
    z_stream zStream;
    memset(&zStream, 0, sizeof(zStream));
    deflateInit2(
        &zStream,
        Z_BEST_SPEED,
        Z_DEFLATED,
        -MAX_WBITS,
        8,
        Z_DEFAULT_STRATEGY);

    // Сжимаем данные
    zStream.avail_in = lfh.uncompressedSize;
    zStream.next_in = (unsigned char*) Data;
    zStream.avail_out = dataBuffer.size();
    zStream.next_out = dataBuffer.data();
    deflate(&zStream, Z_FINISH);

    // Размер сжатых данных
    lfh.compressedSize = zStream.total_out;
    lfh.compressionMethod = Z_DEFLATED;

    // Очистка
    deflateEnd(&zStream);

    // Длина имени файла
    lfh.filenameLength = filename.size();

    // Сохраним смещение к записи Local File Header внутри архива
    const uint32_t lfhOffset = os.tellp();

    // Запишем сигнатуру Local File Header
    const uint32_t signature = 0x04034b50;
    os.write((char *) &signature, sizeof(signature));

    // Запишем Local File Header
    os.write((char *) &lfh, sizeof(lfh));

    // Запишем имя файла
    os.write(filename.c_str(), filename.size());

    // Запишем данные
    os.write((char *) dataBuffer.data(), lfh.compressedSize);

    // Сохраним все данные для Central directory file header
    FileInfo fileInfo;
    fileInfo.compressedSize = lfh.compressedSize;
    fileInfo.uncompressedSize = lfh.uncompressedSize;
    fileInfo.compressionMethod = lfh.compressionMethod;
    fileInfo.crc32 = lfh.crc32;
    fileInfo.offset = lfhOffset;
    fileInfoList.push_back(fileInfo);
}
