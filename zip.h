#ifndef ZIP_H
#define ZIP_H

#include <iostream>
#include <stdint.h>
#include <vector>

struct FileInfo
{
    uint32_t compressedSize;
    uint32_t uncompressedSize;
    uint16_t compressionMethod;
    uint32_t crc32;
    uint32_t offset;
};

class Zip
{
public:
    // Конструктор. Создает архив
    // File - файловый поток, уже открытый на запись (std::ios_base::out | std::ios_base::binary)
    Zip(std::ofstream &File);

    ~Zip();

    // Добавление файла из памяти в архив
    // FileName - имя файла в архиве
    // Data - содержимое файла
    // DataSize - объем содержимого файла, в байтах
    void Add(char const *FileName, char const *Data, size_t DataSize);
private:
    // Поток на запись
    std::ofstream &os;
    // Буфер для сжатых данных
    std::vector<uint8_t> dataBuffer;
    // Информация для создания Central directory file header
    std::vector<FileInfo> fileInfoList;
    // Список файлов
    std::vector<std::string> filenames;

};

#endif // ZIP_H
